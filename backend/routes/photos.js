const express = require('express');
const Photo = require('../models/Photo');
const auth = require('../middleware/auth');
const multer = require("multer");
const path = require('path');
const config = require('../config');
const {nanoid} = require("nanoid");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },

    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});
const router = express.Router();

router.get('/', async (req, res) => {
   try {
       const photos = await Photo.find().populate('userId', 'displayName');

       res.send(photos);
   } catch (error) {
       res.sendStatus(500);
   }
});

router.get('/:id', async (req, res) => {
   try {
       const userPhotos = await Photo.find({userId: req.params.id}).populate('userId', 'displayName');
       res.send(userPhotos);
   } catch (error) {
       res.sendStatus(500);
   }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        const photoData = {
            userId: req.user._id,
            title: req.body.title,
        };

        if (req.file) {
            photoData.image = 'uploads/' + req.file.filename;
        }

        const photo = new Photo(photoData);
        await photo.save();

        res.send(photo);
    } catch (error) {
        res.status(400).send(error);
    }
});

router.delete('/:id', auth, async (req, res) => {
   try {
        const photo = await Photo.findByIdAndDelete(req.params.id);

        if (!photo) {
            return res.status(404).send({global: 'Not Found'});
        }

        res.send(photo);

   } catch (error) {
       res.sendStatus(500);
   }
});

module.exports = router;