const mongoose = require('mongoose');
const User = require('./models/User');
const Photo = require('./models/Photo');
const config = require('./config');
const {nanoid} = require('nanoid');

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const collection of collections) {
        await mongoose.connection.db.dropCollection(collection.name);
    }

    const [firstUser, secondUser] = await User.create({
        email: 'firstuser@gmail.com',
        password: '123',
        token: nanoid(),
        displayName: 'First User',
        avatar: '/fixtures/firstUserAvatar.jpg',
    }, {
        email: 'seconduser@gmail.com',
        password: '123',
        token: nanoid(),
        displayName: 'Second User',
        avatar: '/fixtures/secondUserAvatar.png',
    });

    await Photo.create({
        userId: firstUser,
        title: 'Outdoors',
        image: 'fixtures/1.webp',
    }, {
        userId: firstUser,
        title: 'Tree',
        image: '/fixtures/2.webp',
    }, {
        userId: firstUser,
        title: 'Guy with Fire',
        image: '/fixtures/3.webp'
    }, {
        userId: secondUser,
        title: 'Rose',
        image: '/fixtures/4.webp'
    }, {
        userId: secondUser,
        title: 'Penguin',
        image: '/fixtures/5.webp'
    }, {
        userId: secondUser,
        title: 'Bones',
        image: '/fixtures/6.webp'
    })

    await mongoose.connection.close();
};

run().catch(e => console.error(e));