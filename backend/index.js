require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const cors = require('cors');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./routes/users');
const photos = require('./routes/photos');

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

app.use('/users', users);
app.use('/photos', photos);

const port = 8000;
const run = async () => {
    await mongoose.connect(config.db.url);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(() => {
        console.log('Exiting')
        mongoose.disconnect();
    });
};

run().catch(e => console.error(e));