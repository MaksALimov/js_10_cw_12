import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FormElement from "../../components/UI/Form/FormElement";
import {Button, TextField} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {clearCreatePhotoErrors, createPhotoRequest} from "../../store/actions/photosActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const AddPhoto = ({history}) => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.photos.createPhotoLoading);
    const error = useSelector(state => state.photos.createPhotoError);

    const goBack = () => {
        history.goBack();
    };

    const [photo, setPhoto] = useState({
        title: '',
        image: null,
    });

    useEffect(() => {
        return () => {
            dispatch(clearCreatePhotoErrors());
        };
    }, [dispatch]);

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setPhoto(prevState => ({...prevState, [name]: value}));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setPhoto(prevState => {
            return {...prevState, [name]: file}
        });
    };

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(photo).forEach(key => {
            formData.append(key, photo[key]);
        });

        dispatch(createPhotoRequest(formData));
    };

    return (
        <Grid
            container
            component="form"
            direction="column"
            onSubmit={submitFormHandler}
            spacing={5}
            noValidate
        >
            <FormElement
                onChange={inputChangeHandler}
                name="title"
                label="Title"
                required
                value={photo.title}
                error={getFieldError('title')}
            />
            <Grid item xs>
                <TextField
                    type="file"
                    name="image"
                    required
                    fullWidth
                    variant="outlined"
                    onChange={fileChangeHandler}
                    error={Boolean(getFieldError('image'))}
                    helperText={getFieldError('image')}
                />
            </Grid>
            <Grid item xs>
                <ButtonWithProgress
                    type="submit"
                    variant="contained"
                    loading={loading}
                    disabled={loading}
                >
                    Create Photo
                </ButtonWithProgress>
            </Grid>
            <Button variant="contained" onClick={goBack}>
                Back
            </Button>
        </Grid>
    );
};

export default AddPhoto;