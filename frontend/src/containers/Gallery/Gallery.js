import React, {useEffect} from 'react';
import {CircularProgress} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getPhotosRequest} from "../../store/actions/photosActions";
import Grid from "@material-ui/core/Grid";
import Photo from "../../components/Photo/Photo";

const Gallery = () => {
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);
    const loading = useSelector(state => state.photos.getPhotosLoading);

    useEffect(() => {
        dispatch(getPhotosRequest());
    }, [dispatch]);

    return (
        <>
            {loading ? <Grid container justifyContent="center"><CircularProgress/></Grid> : (

                <Grid container justifyContent="space-between">
                    {photos.map(photo => (
                        <Photo
                            key={photo._id}
                            userId={photo.userId._id}
                            image={photo.image}
                            username={photo.userId.displayName}
                            title={photo.title}
                        />
                    ))}
                </Grid>
            )}
        </>
    );
};

export default Gallery;