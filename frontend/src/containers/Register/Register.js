import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import {clearTextFieldsErrors, registerUserRequest} from "../../store/actions/usersAction";
import {Avatar, Container, Grid, makeStyles, Typography} from "@material-ui/core";
import IconOutlinedIcon from '@material-ui/icons/LockOutlined'
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
    container: {
        marginTop: '50px',
    },

    paper: {
        marginTop: "150px",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },

    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },

    signInInscription: {
        fontSize: '20px',
        fontWeight: 'bold',
    },
}));

const Register = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.users.registerUserError);
    const loading = useSelector(state => state.users.registerUserLoading);

    const [user, setUser] = useState({
        email: '',
        password: '',
        displayName: '',
        avatarImage: '',
    });

    useEffect(() => {
        return () => {
            dispatch(clearTextFieldsErrors());
        };
    }, [dispatch]);

    const inputChangeHandler = e => {
        const {name, value} = e.target;
        setUser(prevState => ({...prevState, [name]: value}));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    const submitFormHandler = e => {
        e.preventDefault();
        dispatch(registerUserRequest({...user}));
    };

    return (
        <Container component="section" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <IconOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h6">
                    Sign up
                </Typography>
                <Grid container
                      component="form"
                      noValidate
                      spacing={3}
                      className={classes.container}
                      direction="column"
                      onSubmit={submitFormHandler}
                >
                    <FormElement
                        type="email"
                        required
                        autoComplete="new-email"
                        label="Email"
                        name="email"
                        value={user.email}
                        onChange={inputChangeHandler}
                        error={getFieldError('email')}
                    />
                    <FormElement
                        type="password"
                        required
                        autoComplete="new-password"
                        label="Password"
                        name="password"
                        value={user.password}
                        onChange={inputChangeHandler}
                        error={getFieldError('password')}
                    />
                    <FormElement
                        type="test"
                        required
                        autoComplete="new-displayName"
                        label="DisplayName"
                        name="displayName"
                        value={user.displayName}
                        onChange={inputChangeHandler}
                        error={getFieldError('displayName')}
                    />

                    <FormElement
                        type="text"
                        autoComplete="new-avatarImage"
                        label="AvatarImage"
                        name="avatarImage"
                        value={user.avatarImage}
                        onChange={inputChangeHandler}
                    />

                    <Grid item xs={12}>
                        <ButtonWithProgress
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            loading={loading}
                            disabled={loading}
                        >
                            Sign Up
                        </ButtonWithProgress>
                    </Grid>
                    <Grid item container justifyContent="flex-end">
                        <RouterLink variant="body1" to="/login" className={classes.signInInscription}>
                            Already have an account? Sign in
                        </RouterLink>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};

export default Register;