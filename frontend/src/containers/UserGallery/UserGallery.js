import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import {getUserPhotoRequest} from "../../store/actions/photosActions";
import Photo from "../../components/Photo/Photo";
import {Button, CircularProgress, makeStyles, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(() => ({
    addPhotoBtn: {
        color: 'inherit',
        textDecoration: 'none',
    },
}));

const UserGallery = ({match, history}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const userPhotos = useSelector(state => state.photos.photos);
    const loading = useSelector(state => state.photos.getUserPhotoLoading);

    const goBack = () => {
        history.goBack();
    };

    useEffect(() => {
        dispatch(getUserPhotoRequest(match.params.id))
    }, [dispatch, match.params.id]);

    return userPhotos && (
        <>
            {loading ? <Grid container justifyContent="center"><CircularProgress/></Grid> : (
                <>
                    <Grid container justifyContent="space-between">
                        <Typography variant="h4">
                            {userPhotos.length === 0 ? (
                                user.displayName + '\'s' + ' Gallery'
                            ) : (
                                userPhotos[0]?.userId?.displayName + '\'s ' + 'Gallery'
                            )}
                        </Typography>
                        {user?._id === match.params.id ? (
                            <>
                                <Button variant="contained">
                                    <RouterLink to="/add-photo" className={classes.addPhotoBtn}>
                                        Add new Photo
                                    </RouterLink>
                                </Button>
                                <Button variant="contained" onClick={goBack}>
                                    Back
                                </Button>
                            </>
                        ) : null}
                    </Grid>
                    <Grid container justifyContent="space-between">
                        {userPhotos.map(userPhoto => (
                            <Photo
                                key={userPhoto._id}
                                photoId={userPhoto._id}
                                image={userPhoto.image}
                                title={userPhoto.title}
                                author={user?._id === match.params.id}
                            />
                        ))}
                    </Grid>
                </>
            )}
        </>
    );
};

export default UserGallery;