import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import {clearTextFieldsErrors, loginUserRequest} from "../../store/actions/usersAction";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {Alert} from "@material-ui/lab";
import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined';
import {Avatar, Container, Grid, makeStyles, Typography} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import GoogleLogin from "../../components/UI/GooleLogin/GoogleLogin";

const useStyles = makeStyles(theme => ({
    container: {
        marginTop: '50px',
    },

    paper: {
        marginTop: "150px",
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },

    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },

    signUpInscription: {
        fontSize: '20px',
        fontWeight: 'bold',
    },

    alert: {
        marginTop: theme.spacing(3),
        width: '100%',
    },
}));

const Login = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const loading = useSelector(state => state.users.loginUserLoading);
    const error = useSelector(state => state.users.loginUserError);

    const [user, setUser] = useState({
        email: '',
        password: '',
    });

    useEffect(() => {
        return () => {
            dispatch(clearTextFieldsErrors())
        };
    }, [dispatch]);

    const onInputChange = e => {
        const {name, value} = e.target;
        setUser(prevState => ({...prevState, [name]: value}));
    };

    const onSubmitForm = e => {
        e.preventDefault();
        dispatch(loginUserRequest({...user}));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Container maxWidth="xs">
            <Grid container
                  direction="column"
                  component="form"
                  onSubmit={onSubmitForm}
                  spacing={3}
            >
                <Grid item className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOpenOutlinedIcon/>
                    </Avatar>
                    <Typography
                        component="h2"
                        variant="h6"
                        className={classes.signUp}
                    >
                        Sign in
                    </Typography>
                    {
                        error &&
                        <Alert severity="error" className={classes.alert}>
                            {error.message || error.global}
                        </Alert>
                    }
                </Grid>
                <FormElement
                    type="text"
                    autoComplete="new-email"
                    label="Email"
                    name="email"
                    value={user.email}
                    onChange={onInputChange}
                    error={getFieldError('email')}
                >
                </FormElement>
                <FormElement
                    type="password"
                    autoComplete="new-password"
                    label="Password"
                    name="password"
                    value={user.password}
                    onChange={onInputChange}
                    error={getFieldError('password')}
                >
                </FormElement>

                <Grid item>
                    <GoogleLogin/>
                </Grid>
                <Grid item>
                    <ButtonWithProgress
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        loading={loading}
                        disabled={loading}
                    >
                        Sign in
                    </ButtonWithProgress>
                </Grid>
                <Grid item container justifyContent="flex-end">
                    <RouterLink variant="body2" to="/register" className={classes.signUpInscription}>
                        Or sign up
                    </RouterLink>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Login;