import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import {apiUrl} from "../../config";
import {deletePhotoRequest, setModal} from "../../store/actions/photosActions";
import {makeStyles, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import Modal from "../UI/Modal/Modal";

const useStyles = makeStyles(() => ({
    imageContainer: {
        width: '450px',
        height: '350px',
        boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
        margin: '20px 10px',
        padding: '20px',
        paddingBottom: '120px',
        textAlign: 'center',
    },

    image: {
        width: '100%',
        height: '100%',
        '&:hover': {
            cursor: 'pointer',
        },
    },

    username: {
        marginTop: '20px',
    }
}));

const Photo = ({image, username, title, userId, author, photoId}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const modal = useSelector(state => state.photos.modal);
    const loading = useSelector(state => state.photos.deletePhotoLoading);

    let photo = apiUrl + '/' + image;

    if (/fixtures/g.test(image)) {
        photo = apiUrl + '/' + image;
    }

    const openModal = (photo) => {
        dispatch(setModal({photo, boolean: true}));
    };

    return (
        <>
            <Grid item className={classes.imageContainer} xs={3}>
                <img
                    className={classes.image}
                    src={photo} alt="photo"
                    onClick={() => openModal(photo)}
                />
                <Typography variant="h5" className={classes.username}>
                    {title}
                </Typography>
                <Typography variant="h5" className={classes.username}>
                    <RouterLink to={`/users/${userId}`}>{username}</RouterLink>
                </Typography>
                {author ? (
                        <ButtonWithProgress
                            loading={loading}
                            disabled={loading}
                            variant="contained"
                            onClick={() => dispatch(deletePhotoRequest({photoId, title}))}
                        >
                            Delete
                        </ButtonWithProgress>
                    ) : null}
            </Grid>
            {modal ? <Modal/> : null}
        </>
    );
};

export default Photo;