import React from 'react';
import {Container, CssBaseline} from "@material-ui/core";
import Header from "../Header/Header";

const Layout = ({children}) => {
    return (
        <>
            <CssBaseline/>
            <Header/>
            <main>
                <Container>
                    {children}
                </Container>
            </main>
        </>
    );
};

export default Layout;