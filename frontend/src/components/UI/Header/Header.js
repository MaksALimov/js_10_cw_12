import React from 'react';
import {useSelector} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import Anonymous from "./Menu/Anonymous";
import UserMenu from "./Menu/UserMenu";
import {AppBar, makeStyles, Toolbar, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(() => ({
    header: {
      padding: '10px',
    },

    mainLink: {
        color: 'inherit',
        fontSize: '50px',
        textDecoration: 'none',
        '&:hover': {
            color: 'inherit'
        }
    },

    logout: {
        fontSize: '20px',
        fontWeight: 'bold',
    },

    displayName: {
        fontSize: '35px',
        fontWeight: 'bold',
        margin: '0 30px',
    },
}))

const Header = () => {
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    return (
        <>
            <AppBar position="fixed" className={classes.header}>
                <Toolbar>
                    <Grid container justifyContent="space-between" alignItems="center">
                        <Grid item>
                            <Typography variant="h6">
                                <RouterLink to="/" className={classes.mainLink}>Photo Gallery</RouterLink>
                            </Typography>
                        </Grid>
                        <Grid item>
                            {user ? <UserMenu/> : <Anonymous/>}
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <Toolbar/>
            <Toolbar/>
            <Toolbar/>
        </>
    );
};

export default Header;