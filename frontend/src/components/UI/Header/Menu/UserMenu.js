import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import {Grid, makeStyles, Typography} from "@material-ui/core";
import {logoutUserRequest} from "../../../../store/actions/usersAction";
import ButtonWithProgress from "../../ButtonWithProgress/ButtonWithProgress";
import {apiUrl} from "../../../../config";

const useStyles = makeStyles(() => ({
    displayName: {
        fontSize: '45px',
        color: 'inherit',
        margin: '0 20px',
        textDecoration: 'none',
    },

    imageContainer: {
        width: '140px',
        height: '140px',
    },

    image: {
        width: '100%',
        height: '100%',
    },
}));

const UserMenu = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const loading = useSelector(state => state.users.logoutUserLoading);

    let avatarImage = user?.avatar;

    if (/fixtures/g.test(user?.avatar)) {
        avatarImage = apiUrl + '/' + user?.avatar;
    }

    return (
        <Grid container spacing={6} alignItems="center">
            <Grid item className={classes.imageContainer}>
                {user.avatar ? <img className={classes.image} src={avatarImage} alt="avatarImage"/> : null}
            </Grid>
            <Grid item>
                <Typography className={classes.displayName}> Hello
                    <RouterLink
                        to={`/users/${user._id}`}
                        className={classes.displayName}
                    >
                        {user.displayName} !
                    </RouterLink>
                </Typography>
            </Grid>
            <Grid item>
                <ButtonWithProgress
                    variant="contained"
                    onClick={() => dispatch(logoutUserRequest())}
                    loading={loading}
                    disabled={loading}
                >
                    Logout
                </ButtonWithProgress>
            </Grid>
        </Grid>
    );
};

export default UserMenu;