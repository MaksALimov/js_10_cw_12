import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {setModal} from "../../../store/actions/photosActions";
import {Button, Grid, makeStyles} from "@material-ui/core";
import Backdrop from "../BackDrop/Backdrop";

const useStyles = makeStyles(() => ({
    modal: {
        position: 'fixed',
        zIndex: '500',
        backgroundColor: 'white',
        width: '70%',
        border: '1px solid #ccc',
        boxShadow: 'rgba(0, 0, 0, 0.4) 0px 2px 4px, rgba(0, 0, 0, 0.3) 0px 7px 13px -3px, rgba(0, 0, 0, 0.2) 0px -3px 0px inset',
        padding: '16px',
        left: '15%',
        top: '30%',
        boxSizing: 'border-box',
        transition: 'all 0.3s ease-out',
    },

    imageContainer: {
        width: '500px',
        height: '500px',
        boxShadow: 'rgba(0, 0, 0, 0.16) 0px 1px 4px, rgb(51, 51, 51) 0px 0px 0px 3px',
        padding: '10px',
    },

    image: {
        width: '100%',
        height: '100%',
    },

    title: {
        fontSize: '28px',
        margin: '10px 0',
    },

    buttons: {
        margin: '10px 0',
    },
}));

const Modal = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const photo = useSelector(state => state.photos.modalImage);

    return (
        <>
            <Backdrop/>
            <Grid container direction="column" className={classes.modal} alignItems="center">
                <Grid item className={classes.imageContainer}>
                    <img className={classes.image} src={photo} alt="image"/>
                </Grid>
                <Button
                    variant="contained"
                    className={classes.buttons}
                    onClick={() => dispatch(setModal(false))}
                >
                    Close
                </Button>
            </Grid>
        </>
    );
};

export default Modal;