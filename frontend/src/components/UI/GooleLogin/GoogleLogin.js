import React from 'react';
import {useDispatch} from "react-redux";
import GoogleLoginButton from "react-google-login";
import {googleClientId} from "../../../config";
import {googleLoginRequest} from "../../../store/actions/usersAction";
import {Button} from "@material-ui/core";
import {FcGoogle} from "react-icons/fc";

const GoogleLogin = () => {
    const dispatch = useDispatch();

    const googleLoginHandler = response => {
        dispatch(googleLoginRequest(response));
    };

    return (
        <GoogleLoginButton
            clientId={googleClientId}
            render={props => (
                <Button
                    fullWidth
                    variant="outlined"
                    startIcon={<FcGoogle/>}
                    color="primary"
                    onClick={props.onClick}
                >
                    Login with Google
                </Button>
            )}
            onSuccess={googleLoginHandler}
            cookiePolicy={'single_host_origin'}
        />
    );
};

export default GoogleLogin;