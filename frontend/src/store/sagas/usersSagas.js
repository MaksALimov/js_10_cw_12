import {takeEvery, put} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {
    googleLoginRequest,
    loginUserFailure, loginUserRequest,
    loginUserSuccess, logoutUserFailure, logoutUserRequest, logoutUserSuccess,
    registerUserFailure,
    registerUserRequest,
    registerUserSuccess
} from "../actions/usersAction";
import {toast} from "react-toastify";

function* registerUsersSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users', userData);
        yield put(registerUserSuccess(response.data));
        toast.success('Register successful', {
            position: "bottom-right",
            autoClose: 3000,
        });
        historyPush('/');
    } catch (error) {
        yield put(registerUserFailure(error.response.data));
        toast.error(error.response.data.global, {
            position: 'top-center',
        });
    }
}

function* loginUsersSaga({payload: userData}) {
    try {
        const response = yield axiosApi.post('/users/sessions', userData);
        yield put(loginUserSuccess(response.data));
        toast.success('Login successful!', {
            position: "bottom-right",
            autoClose: 3000,
        });
        historyPush('/');
    } catch (error) {
        yield put(loginUserFailure(error.response.data));
    }
}

function* logoutUsersSaga() {
    try {
        yield axiosApi.delete('/users/sessions');
        yield put(logoutUserSuccess());
        toast.success('Logged out!', {
            position: "bottom-right",
            autoClose: 3000,
        });
    } catch (error) {
        yield put(logoutUserFailure(error.response.data));
        toast.error(error.response.data.global, {
            position: "top-center",
        });
    }
}

function* loginGoogleSaga({payload: googleData}) {
    try {
        const response = yield axiosApi.post('/users/googleLogin', {
            tokenId: googleData.tokenId,
            googleId: googleData.googleId,
        });
        yield put(loginUserSuccess(response.data));
        toast.success('Login successful!', {
            position: 'bottom-right',
            autoClose: 3000,
        });
        historyPush('/');
    } catch (error) {
        yield put(loginUserFailure(error.response.data));
        toast.error(error.response.data.global, {
            position: 'top-center',
        });
    }
}

const usersSaga = [
    takeEvery(registerUserRequest, registerUsersSaga),
    takeEvery(loginUserRequest, loginUsersSaga),
    takeEvery(logoutUserRequest, logoutUsersSaga),
    takeEvery(googleLoginRequest, loginGoogleSaga),
];

export default usersSaga;