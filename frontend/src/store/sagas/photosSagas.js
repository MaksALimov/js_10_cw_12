import {takeEvery, put} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";
import {
    createPhotoFailure,
    createPhotoRequest,
    createPhotoSuccess, deletePhotoFailure, deletePhotoRequest, deletePhotoSuccess,
    getPhotosFailure, getPhotosRequest,
    getPhotosSuccess, getUserPhotoFailure, getUserPhotoRequest, getUserPhotoSuccess
} from "../actions/photosActions";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";

function* createPhotosSaga({payload: photoData}) {
    try {
        const response = yield axiosApi.post('/photos', photoData);
        yield put(createPhotoSuccess(response.data));
        toast.success('You successfully added Photo!', {
            position: 'bottom-right',
            autoClose: 3000,
        });
        historyPush('/');
    } catch (error) {
        yield put(createPhotoFailure(error.response.data));
        toast.error(error.response.data.global, {
            position: 'top-center',
        });
    }
}

function* getPhotosSaga() {
    try {
        const response = yield axiosApi.get('/photos');
        yield put(getPhotosSuccess(response.data));
    } catch (error) {
        yield put(getPhotosFailure(error.response.data));
        toast.error(error.response.data.global || error.response.statusText, {
            position: 'top-center',
        });
    }
}

function* getUserPhotoSaga({payload: userId}) {
    try {
        const response = yield axiosApi.get(`/photos/${userId}`);
        yield put(getUserPhotoSuccess(response.data));
    } catch (error) {
        yield put(getUserPhotoFailure(error.response.data));
        toast.error(error.response.data.global, {
            position: 'top-center',
        });
    }
}

function* deletePhotoSaga({payload: photoData}) {
    try {
        yield axiosApi.delete(`/photos/${photoData.photoId}`);
        yield put(deletePhotoSuccess(photoData.photoId));
        toast.success(`You deleted ${photoData.title}`, {
            position: 'bottom-right',
            autoClose: 3000,
        });
    } catch (error) {
        yield put(deletePhotoFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

const photosSaga = [
    takeEvery(createPhotoRequest, createPhotosSaga),
    takeEvery(getPhotosRequest, getPhotosSaga),
    takeEvery(getUserPhotoRequest, getUserPhotoSaga),
    takeEvery(deletePhotoRequest, deletePhotoSaga)
];

export default photosSaga;