export const saveToLocalStorage = state => {
    try {
        const serialized = JSON.stringify(state);
        localStorage.setItem('cw-12', serialized);
    } catch (e) {
        console.log('Could not save state');
    }
};

export const loadFromLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('cw-12');

        if (serializedState === null) {
            return undefined;
        }

        return JSON.parse(serializedState);

    } catch (e) {
        return undefined;
    }
};