import userSlice from "../slices/usersSlice";

export const {
    registerUserRequest,
    registerUserSuccess,
    registerUserFailure,
    loginUserRequest,
    loginUserSuccess,
    loginUserFailure,
    googleLoginRequest,
    logoutUserRequest,
    logoutUserSuccess,
    logoutUserFailure,
    clearTextFieldsErrors,
} = userSlice.actions;