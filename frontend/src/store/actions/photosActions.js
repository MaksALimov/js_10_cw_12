import photoSlice from '../slices/photosSlice';

export const {
    createPhotoRequest,
    createPhotoSuccess,
    createPhotoFailure,
    getPhotosRequest,
    getPhotosSuccess,
    getPhotosFailure,
    getUserPhotoRequest,
    getUserPhotoSuccess,
    getUserPhotoFailure,
    deletePhotoRequest,
    deletePhotoSuccess,
    deletePhotoFailure,
    clearCreatePhotoErrors,
    setModal,
} = photoSlice.actions;