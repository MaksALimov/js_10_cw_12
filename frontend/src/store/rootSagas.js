import {all} from 'redux-saga/effects';
import usersSagas from "./sagas/usersSagas";
import photosSaga from "./sagas/photosSagas";

export function* rootSagas() {
    yield all([
        ...usersSagas,
        ...photosSaga
    ]);
}