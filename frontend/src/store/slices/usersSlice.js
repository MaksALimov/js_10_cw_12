import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    user: null,
    registerUserLoading: false,
    registerUserError: null,
    loginUserLoading: false,
    loginUserError: null,
    logoutUserLoading: false,
    logoutUserError: null,
};

const userSlice = createSlice({
    name: 'users',
    initialState,

    reducers: {
        registerUserRequest(state) {
            state.registerUserLoading = true;
        },

        registerUserSuccess(state, {payload: user}) {
            state.user = user;
            state.registerUserLoading = false;
        },

        registerUserFailure(state, {payload: registerError}) {
            state.registerUserLoading = false;
            state.registerUserError = registerError;
        },

        loginUserRequest(state) {
            state.loginUserLoading = true;
        },

        loginUserSuccess(state, {payload: userData}) {
            state.user = userData;
            state.loginUserLoading = false;
        },

        loginUserFailure(state, {payload: loginError}) {
            state.loginUserError = loginError;
            state.loginUserLoading = false;
        },

        logoutUserRequest(state) {
            state.logoutUserLoading = true;
        },

        logoutUserSuccess(state) {
            state.user = null;
            state.logoutUserLoading = false;
        },

        googleLoginRequest(state) {
            return state;
        },

        logoutUserFailure(state, {payload: logoutError}) {
            state.logoutUserLoading = false;
            state.logoutUserError = logoutError;
        },

        clearTextFieldsErrors(state) {
            state.registerUserError = null;
            state.loginUserError = null;
        }
    },
});

export default userSlice;