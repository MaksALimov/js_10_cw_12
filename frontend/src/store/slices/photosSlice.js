import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    photos: [],
    getPhotosLoading: false,
    getPhotosError: null,
    createPhotoLoading: false,
    createPhotoError: null,
    getUserPhotoLoading: false,
    getUserPhotoError: null,
    deletePhotoLoading: false,
    deletePhotoError: null,
    modal: false,
    modalImage: null,
};

const photoSlice = createSlice({
    name: 'photos',
    initialState,

    reducers: {
        createPhotoRequest(state) {
            state.createPhotoLoading = true;
        },

        createPhotoSuccess(state) {
            state.createPhotoLoading = false;
        },

        createPhotoFailure(state, {payload: createPhotoError}) {
            state.createPhotoLoading = false;
            state.createPhotoError = createPhotoError;
        },

        getPhotosRequest(state) {
            state.getPhotosLoading = true;
        },

        getPhotosSuccess(state, {payload: photos}) {
            state.photos = photos;
            state.getPhotosLoading = false;
        },

        getPhotosFailure(state, {payload: getPhotosError}) {
            state.getPhotosLoading = false;
            state.getPhotosError = getPhotosError;
        },

        getUserPhotoRequest(state) {
            state.getUserPhotoLoading = true;
        },

        getUserPhotoSuccess(state, {payload: userPhotos}) {
            state.getUserPhotoLoading = false;
            state.photos = userPhotos;
        },

        getUserPhotoFailure(state, {payload: userPhotoError}) {
            state.getUserPhotoLoading = false;
            state.getUserPhotoError = userPhotoError;
        },

        deletePhotoRequest(state) {
            state.deletePhotoLoading = true;
        },

        deletePhotoSuccess(state, {payload: photoId}) {
            state.deletePhotoLoading = false;
            state.photos = state.photos.filter(photo => photo._id !== photoId);
        },

        deletePhotoFailure(state, {payload: error}) {
            state.deletePhotoLoading = false;
            state.deletePhotoError = error;
        },

        setModal(state, {payload}) {
            state.modalImage = payload.photo;
            state.modal = payload.boolean;
        },

        clearCreatePhotoErrors(state) {
            state.createPhotoError = null;
        },
    },
});

export default photoSlice;