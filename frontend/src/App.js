import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Gallery from "./containers/Gallery/Gallery";
import Login from "./containers/Login/Login";
import Layout from "./components/UI/Layout/Layout";
import UserGallery from "./containers/UserGallery/UserGallery";
import AddPhoto from "./containers/AddPhoto/AddPhoto";
import {useSelector} from "react-redux";

const App = () => {
    const user = useSelector(state => state.users.user);

    const Permit = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Gallery}/>
                <Route path="/users/:id" component={UserGallery}/>
                <Permit
                    path="/add-photo"
                    isAllowed={user}
                    component={AddPhoto}
                    redirectTo="/"
                />
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
            </Switch>
        </Layout>
    )
};

export default App;
